/* Copyright© 2000 - 2019 SuperMap Software Co.Ltd. All rights reserved.
 * This program are made available under the terms of the Apache License, Version 2.0
 * which accompanies this distribution and is available at http://www.apache.org/licenses/LICENSE-2.0.html.*/
import {SuperMap} from './SuperMap';

import {
    Collection,
    Curve,
    GeoText,
    LinearRing,
    LineString,
    MultiLineString,
    MultiPoint,
    MultiPolygon,
    GeometryPoint,
    Polygon,
    Rectangle,
    StringExt,
    NumberExt,
    FunctionExt,
    ArrayExt,
    Bounds,
    Credential,
    DateExt,
    Event,
    Events,
    Feature,
    Geometry,
    LonLat,
    Pixel,
    Size,
    CommonUtil,
    GeometryVector
} from './commontypes';

export {
    Collection,
    Curve,
    GeoText,
    LinearRing,
    LineString,
    MultiLineString,
    MultiPoint,
    MultiPolygon,
    GeometryPoint,
    Polygon,
    Rectangle,
    StringExt,
    NumberExt,
    FunctionExt,
    ArrayExt,
    Bounds,
    Credential,
    DateExt,
    Event,
    Events,
    Feature,
    Geometry,
    LonLat,
    Pixel,
    Size,
    CommonUtil,
    GeometryVector
};